-- Summary --

Some European countries require that invoices are issued with consecutive
invoice numbers. This means that the order number cannot be used as an invoice
number since there are generally "holes" in the sequence, e.g. when an order
gets cancelled for some reason.

This module attempts to fix this by providing an "Invoice number" in addition
to the "Order id".


-- Features --

Extends Ubercart 2.x to provide

* Invoice number (in addition to the order id)
* Button which the shop owner can click when an invoice needs to be generated for an order
* Consecutive invoice numbers 1,2,3...
* Invoice date
* Invoice template that includes invoice date and invoice number


-- Installation --

* Copy the module folder to sites/all/modules
* Enable the module
* There is a template which can be used as a starting point for invoices
		uc_invoice/templates/uc_order-invoice-number.tpl.php
  If you want to modify this file you may copy it to your current theme folder first.
* Go to "Order settings"
		admin/store/settings/orders/edit/basic
	and set "On-site invoice template" to "invoice-number"


-- Usage --

At the bottom of each order page
	admin/store/orders/*

There is a button: "Generate invoice". When the button is clicked an invoice number
is automatically generated for the order.

-- Authors --

This module did see the light of day at http://www.ubercart.org/forum/support/15896/invoice_number_vs_order_number.
Created by koyama (http://www.ubercart.org/user/10212). Modified by cfab (http://www.ubercart.org/user/9637), derjochenmeyer (http://www.ubercart.org/user/17196) and fuerst (http://drupal.org/user/59702).
Currently maintained by Bernhard Fürst <bernhard.fuerst@fuerstnet.de>, http://drupal.org/user/59702