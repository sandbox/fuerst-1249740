<?php
// $Id$

/**
 * @file
 * This file contains the Conditional Actions hooks and functions necessary to
 * make the invoice generation actions work.
 * @date 23/09/2010
 * @author TahitiClic
 */

/******************************************************************************
 * Conditional Actions Hooks
 */

/**
 * Implementation of hook_ca_action().
 */
function uc_invoice_ca_action() {
  $order_arg = array(
    '#entity' => 'uc_order',
    '#title' => t('Order'),
  );

  $actions['uc_invoice_generate_invoice'] = array(
    '#title' => t('Generate an invoice for an order'),
    '#category' => t('Invoice'),
    '#callback' => 'uc_invoice_generate_invoice',
    '#arguments' => array(
      'order' => $order_arg,
    ),
  );

  return $actions;
}

/******************************************************************************
 * Action Callbacks and Forms
 */

/**
 * Generate the invoice
 *
 * @see uc_invoice_order(), uc_invoice_view_generate_invoice_form_submit()
 */
function uc_invoice_generate_invoice(&$order) {

  // we check if there's already an invoice for this order, if yes we do nothing
  // elsewise we generate the invoice
  $result = db_query("SELECT COUNT(*) FROM {uc_invoices} WHERE order_id = %d", $order->order_id);
  $count = db_result($result);
  if ($count == 0) {
  $order_id = $order->order_id;
  $time = time();
  db_query("INSERT INTO {uc_invoices} (invoice_id, order_id, created) VALUES (NULL, %d, %d)", $order_id, $time);
  }
}